#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from flask import Flask
from . import settings, restengine
from .models import db


def create_app():
    app = Flask(__name__)
    app.config.from_object(settings)
    db.init_app(app)
    restengine.init_app(app)
    return app
