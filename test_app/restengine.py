#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from flask_restengine.core import MongoAPI
from .models import User, Content, Post


def init_app(app):
    api = MongoAPI()
    api.register(User, 'user')
    api.register(Content, 'content')
    api.register(Post, 'post')
    api.init_app(app)
