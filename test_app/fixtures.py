#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from datetime import datetime
from bson.tz_util import utc

user_1 = {
    'name': 'andrew',
    'emails': ['a@b.com'],
    'datetime': datetime(2014, 12, 19, 16, 04, 30, tzinfo=utc)
}

user_2 = {
    'name': 'rodionoff',
    'emails': ['b@b.com'],
    'datetime': datetime(2014, 12, 20, 16, 04, 30, tzinfo=utc)
}

post_1 = {
    'title': 'first post!',
    # author
    'tags': ['tag1', 'tag2', 'tag3'],
    # user_lists
    'sections': [
        {'text': 'this is the first section of the first post.',
            'lang': 'en'},
        {'text': 'this is the second section of the first post.',
            'lang': 'de'},
        {'text': 'this is the third section of the first post.',
            'lang': 'fr'},
    ],
    'content': {
        'text': 'this is the content for my first post.',
        'lang': 'cn',
    },
    'is_published': True,
}

post_2 = {
    'title': 'Second post',
    'is_published': False,
}
