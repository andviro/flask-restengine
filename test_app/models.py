#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from flask_mongoengine import MongoEngine

db = MongoEngine()


class User(db.Document):
    name = db.StringField(max_length=50)
    emails = db.ListField(db.EmailField())
    datetime = db.DateTimeField()


class Content(db.EmbeddedDocument):
    text = db.StringField()
    lang = db.StringField(max_length=3)


class Post(db.Document):
    title = db.StringField(max_length=120, required=True)
    author = db.ReferenceField(User)
    tags = db.ListField(db.StringField(max_length=30))
    sections = db.ListField(db.EmbeddedDocumentField(Content))
    content = db.EmbeddedDocumentField(Content)
    is_published = db.BooleanField()
