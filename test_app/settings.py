#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function

MONGODB_SETTINGS = {
    'DB': 'restengine_test_db',
    'HOST': 'localhost',
    'TZ_AWARE': True,
}
