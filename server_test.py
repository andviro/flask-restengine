#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals, print_function
import os
from test_app.factory import create_app


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 8000))
    app = create_app()
    app.config['TESTING'] = True
    # client = app.test_client()
    # print(client.get('/db/user/5493d3224472a3595e775c71').data)
    app.run(host='0.0.0.0', port=port)
