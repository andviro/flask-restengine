#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ez_setup
ez_setup.use_setuptools()

from setuptools import setup

depends = [
    "Flask", "Flask-Mongoengine", "Mimerender", "Webargs", "Flask-Jsonpify"
]

setup_params = dict(
    name='Flask-Restengine',
    version='0.0.1',
    packages=['flask_restengine'],
    author='Andrew Rodionoff',
    author_email='andviro@gmail.com',
    license='BSD',
    description='A thin RESTish wrapper around Mongoengine and Flask',
    install_requires=depends,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)

setup(**setup_params)
