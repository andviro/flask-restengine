#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from mongoengine.fields import *  # noqa
from bson.json_util import object_hook as bson_hook
_bson_keys = set((
    "$oid",
    "$ref",
    "$date",
    "$regex",
    "$minKey",
    "$maxKey",
    "$binary",
    "$code",
    "$uuid",
))

_list_fields = (ListField, SortedListField)
_doc_fields = (EmbeddedDocumentField,
               GenericEmbeddedDocumentField,
               ReferenceField,
               GenericReferenceField)


def field_value(field, value):
    '''
    Converts a supplied value to the type required by the field.
    If the field requires a EmbeddedDocument the EmbeddedDocument
    is created and updated using the supplied data.
    '''
    if field.__class__ in _list_fields:
        return [field_value(field.field, item)
                for item in value]
    elif field.__class__ in _doc_fields:
        model = field.document_type
        return model(**prepare_data(model, value))

    return value


def prepare_data(model, data):
    res = {}
    for key, value in data.iteritems():
        if key in model._fields:
            res[key] = field_value(model._fields[key], value)
        else:
            # handle invalid key
            pass
    return res


def _make_decoder(cls):
    class CustomJSONDecoder(cls):

        def __init__(self, *args, **kwargs):
            self._old_hook = kwargs.pop('object_hook', lambda x: x)
            kwargs['object_hook'] = self.object_hook
            super(cls, self).__init__(*args, **kwargs)

        def object_hook(self, dct):
            keys = set(dct.keys())
            if keys & _bson_keys:
                return bson_hook(dct)
            return self._old_hook(dct)

    return CustomJSONDecoder


def override_json_decoder(app):
    app.json_decoder = _make_decoder(app.json_decoder)
