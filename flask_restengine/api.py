#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from .core import MongoAPI
from ..models.users import User


def init_app(app):
    api = MongoAPI()
    api.register(User)
    api.init_app(app)
