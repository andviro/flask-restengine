#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals, print_function
from mimerender import FlaskMimeRender
import mongoengine

from flask import Blueprint, request, json, url_for
from flask.views import MethodView
from flask_jsonpify import jsonpify
from webargs import Arg
from webargs.flaskparser import use_kwargs

from .helpers import prepare_data, override_json_decoder
import math

import logging
logger = logging.getLogger(__name__)

mimerender = FlaskMimeRender()(default='json', charset='utf-8', json=jsonpify)


class BaseModelView(MethodView):
    decorators = [mimerender]

    def __init__(self, model, base, *args, **kwargs):
        self.model = model
        self.base = base
        super(BaseModelView, self).__init__(*args, **kwargs)

    def url_for(self, obj=None):
        return '{0}/{1}'.format(
            url_for('.{0}'.format(self.base)),
            obj.id if obj else '')

    def get_request_data(self):
        data = request.get_json(force=True, silent=True)
        return data or request.form.to_dict()

    def post(self):
        data = self.get_request_data()
        try:
            data = prepare_data(self.model, data)
            obj = self.model.objects.create(**data)
        except mongoengine.ValidationError as e:
            return dict(error=e.message), 400
        except Exception as e:
            return dict(error=str(e)), 400
        return dict(result=obj), "201 Created", {'Location': self.url_for(obj)}

    def put(self, uid):
        data = self.get_request_data()
        try:
            obj = self.model.objects(id=uid).update(**data)
        except mongoengine.ValidationError as e:
            return dict(error=e.message), 400
        except Exception as e:
            return dict(error=str(e)), 400
        return dict(result=obj)

    def get(self, uid):
        return self._get_one(uid) if uid else self._get_many()

    def _get_one(self, uid):
        try:
            obj = self.model.objects.get(id=uid)
        except mongoengine.DoesNotExist as e:
            return dict(error=e.message), 404
        return dict(result=obj)

    @use_kwargs({
        'page': Arg(int, default=1),
        'results_per_page': Arg(int, default=10),
        'q': Arg(str, default=None),
        'sort': Arg(str, default=None),
    })
    def _get_many(self, page, q, results_per_page, sort):
        params = {} if q is None else json.loads(q)
        queryset = self.model.objects(**params)
        sort = [] if sort is None else json.loads(sort)
        for s in sort:
            queryset = queryset.order_by(s)
        num_results = len(queryset)
        num_pages = int(math.ceil(num_results / results_per_page))
        if page < 1 or not num_pages:
            page = 1
        elif page > num_pages:
            page = num_pages
        objects = queryset[(page - 1) * results_per_page: page * results_per_page]
        return dict(objects=objects, page=page, total_pages=num_pages,
                    num_results=num_results, results_per_page=results_per_page)

    def delete(self, uid):
        try:
            self.model.objects.get(id=uid).delete()
        except mongoengine.DoesNotExist as e:
            return e.message, 404
        return dict(ok=True)

    @classmethod
    def register(cls, bp, model, name):
        base_endpoint = '/{0}'.format(name)
        api_view = cls.as_view(str(name), model, name)
        instance_methods = {'GET', 'POST'}
        single_instance_methods = {'PUT', 'GET', 'DELETE'}
        bp.add_url_rule(base_endpoint, methods=instance_methods,
                        defaults={'uid': None},
                        view_func=api_view)
        bp.add_url_rule('{0}/<uid>'.format(base_endpoint),
                        methods=single_instance_methods,
                        view_func=api_view)
        return api_view


class MongoAPI(object):

    def __init__(self, app=None, *args, **kwargs):
        api_prefix = kwargs.pop('url_prefix', '/db')
        self.bp = Blueprint(
            api_prefix.strip('/').replace('/', '.'), __name__, url_prefix=api_prefix)
        self.view_cls = kwargs.pop('view_cls', BaseModelView)
        if app:
            self.init_app(app, *args, **kwargs)

    def register(self, model, name=None):
        name = name or model._meta.get('collection') or model.__class__.__name__.lower()
        self.view_cls.register(self.bp, model, name)

    def init_app(self, app, *args, **kwargs):
        app.url_map.strict_slashes = False
        app.register_blueprint(self.bp)
        override_json_decoder(app)
