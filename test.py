#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals, print_function
from pies.overrides import *  # noqa
from nose.tools import raises
from test_app.factory import create_app
from test_app.models import User, Post
from test_app.fixtures import user_1, user_2
import bson.json_util as json
# from flask import json


def setup():
    global app, client
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()

    with app.app_context():
        User.drop_collection()
        Post.drop_collection()


def test_post():
    for user in (user_1, user_2):
        resp = client.post('/db/user/', data=json.dumps(user))
        assert "Location" in resp.headers
        loc1 = resp.headers["Location"]
        assert "/db/user/" in loc1
        assert loc1.startswith("http")
        assert 200 <= resp.status_code < 300
        user_obj = json.loads(resp.data)['result']
        assert all(user_obj[k] == v for k, v in user.iteritems())


def test_invalid_post():
    bad_user = dict(user_1)
    bad_user['emails'] = ['aslkajsdkasdjlas']
    resp = client.post('/db/user/', data=json.dumps(bad_user))
    data = json.loads(resp.data)
    assert 'error' in data
    assert data['error'].startswith('ValidationError')


def test_get():
    resp = client.get('/db/user/')
    result = json.loads(resp.data)
    assert len(result['objects']) == result['num_results']

    for user in result['objects']:
        data = json.loads(client.get('/db/user/{0}'.format(user['_id'])).data)
        assert 'result' in data
        user_obj = data['result']
        assert all(user_obj[k] == v for k, v in user.iteritems())


if __name__ == '__main__':
    import nose
    import sys
    from nose.config import Config
    import logging
    logging.basicConfig(level=logging.INFO)
    cfg = Config(logStream=sys.stdout)
    nose.runmodule(config=cfg)
